package at.spengergasse.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpleTest {

    @Test
    public void testAddition() {
        int a = 4;
        int b = 24;

        Assertions.assertEquals(28, a+b);
    }
}
