package at.spengergasse.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SimpleTest {

    @Test
    public void testAddition() {
        int a = 7;
        int b = 21;

        Assertions.assertEquals(28, a+b);
    }
}
